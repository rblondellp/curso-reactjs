import React, { useState, useEffect } from 'react';

const Pokemon = ({ avatar, name }) => {
    return (
        <figure>
            <img src={avatar} alt={name} />
            <figcaption>{name}</figcaption>
        </figure>
    );
};

const AjaxHooks = () => {
    const [pokemons, setPokemons] = useState([]);

    // useEffect(() => {
    //     let url = 'https://pokeapi.co/api/v2/pokemon/';
    //     fetch(url)
    //         .then(res => res.json())
    //         .then(json => {
    //             // console.log(json);
    //             let pokemons = [];
    //             json.results.forEach(el => {
    //                 // console.count();
    //                 fetch(el.url)
    //                     .then(res => res.json())
    //                     .then(json => {
    //                         let pokemon = {
    //                             id: json.id,
    //                             name: json.name,
    //                             avatar: json.sprites.front_default,
    //                         };

    //                         setPokemons(pokemons => [...pokemons, pokemon]);
    //                     });
    //             });
    //         })
    //         .catch(e => console.error(e));
    // }, []);

    useEffect(() => {
        const getPokemons = async url => {
            let res = await fetch(url);
            let json = await res.json();

            json.results.forEach(async el => {
                let res = await fetch(el.url);
                let json = await res.json();
                let pokemon = {
                    id: json.id,
                    name: json.name,
                    avatar: json.sprites.front_default,
                };
                setPokemons(pokemons => [...pokemons, pokemon]);
            });
        };

        let url = 'https://pokeapi.co/api/v2/pokemon/';
        getPokemons(url);
    }, []);

    return (
        <>
            <h2>Peticiones asíncronas con Hooks</h2>
            {pokemons.length === 0 ? (
                <h3>Cargando</h3>
            ) : (
                pokemons.map(el => (
                    <Pokemon key={el.id} name={el.name} avatar={el.avatar} />
                ))
            )}
        </>
    );
};

export default AjaxHooks;
